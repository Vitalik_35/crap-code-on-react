import { useState } from "react";


function JobField({ formData, handleInputChange, jobId, handleDeleteJob }) {
    const fieldNamePrefix = `job-${jobId}`;
    const [emissions, setEmissions] = useState(
        formData[`${fieldNamePrefix}-emissions`] || [{ emissionType: '', emissionCount: '' }]
    );

    const handleChange = (e, fieldName) => {
        const { value } = e.target;
        handleInputChange(fieldName, value);
    };

    const handleAddEmission = () => {
        const newEmission = { emissionType: '', emissionCount: '' };
        setEmissions((prevEmissions) => [...prevEmissions, newEmission]);
        handleInputChange(`${fieldNamePrefix}-emissions`, [
            ...emissions,
            newEmission,
        ]);
    };

    const handleDeleteEmission = (emissionIndex) => {
        setEmissions((prevEmissions) => {
            const updatedEmissions = [...prevEmissions];
            updatedEmissions.splice(emissionIndex, 1);
            return updatedEmissions;
        });
        handleInputChange(`${fieldNamePrefix}-emissions`, emissions);
    };

    return (
        <div key={jobId}>
            <div>
                <label htmlFor={`${fieldNamePrefix}-type`}><b>Вид роботи:</b></label>
                <button onClick={() => handleDeleteJob(jobId)} className="delete-button">
                    -
                </button>

                <input
                    type="text"
                    id={`${fieldNamePrefix}-type`}
                    name={`${fieldNamePrefix}-type`}
                    value={formData[`${fieldNamePrefix}-type`] || ''}
                    onChange={(e) => handleChange(e, `${fieldNamePrefix}-type`)}
                />
            </div>
            <div>
                {emissions.map((emission, emissionIndex) => (
                    <div key={`${jobId}-emission-${emissionIndex}`}>
                        <br/>
                        <label><b>Вид викидів:</b></label>
                        <button
                            onClick={() => handleDeleteEmission(emissionIndex)}
                            className="delete-button"
                        >
                            -
                        </button>
                        <br/>
                        <input
                            type="text"
                            className={'input-custom'}
                            value={emission.emissionType}
                            onChange={(e) => {
                                const updatedEmissions = [...emissions];
                                updatedEmissions[emissionIndex].emissionType = e.target.value;
                                setEmissions(updatedEmissions);
                                handleInputChange(
                                    `${fieldNamePrefix}-emissions`,
                                    updatedEmissions
                                );
                            }}
                        />
                        Кількість:
                        <input
                            type="text"
                            className={'input-custom'}
                            value={emission.emissionCount}
                            onChange={(e) => {
                                const updatedEmissions = [...emissions];
                                updatedEmissions[emissionIndex].emissionCount = e.target.value;
                                setEmissions(updatedEmissions);
                                handleInputChange(
                                    `${fieldNamePrefix}-emissions`,
                                    updatedEmissions
                                );
                            }}
                        />
                    </div>
                ))}
                <button onClick={handleAddEmission}>Додати вид викиду</button>
            </div>
        </div>
    );
}

export default JobField;