import { useState } from 'react';
import JobField from "./JobField.jsx";
import axios from 'axios';

function Form() {
    const [formData, setFormData] = useState({});
    const [jobFields, setJobFields] = useState([]);


    function generateRandomKey() {
        return Math.random().toString(36).substr(2, 9);
    }

    const handleInputChange = (fieldName, value) => {
        setFormData((prevFormData) => ({
            ...prevFormData,
            [fieldName]: value,
        }));
    };

    const handleAddJob = () => {
        const newJobId = generateRandomKey();
        const newFormData = {
            ...formData,
            [`${newJobId}-type`]: '',
            [`${newJobId}-emissions`]: [{ emissionType: '', emissionCount: '' }],
        };

        setFormData(newFormData);
        setJobFields((prevJobFields) => [...prevJobFields, newJobId]);
    };

    const handleDeleteJob = (jobId) => {
        if (jobFields.length === 1) {
            return;
        }

        const updatedJobFields = jobFields.filter((job) => job !== jobId);
        const updatedFormData = { ...formData };

        delete updatedFormData[`${jobId}-type`];
        delete updatedFormData[`${jobId}-emissions`];

        // Додавання логіки видалення порожніх даних
        Object.keys(updatedFormData).forEach((fieldName) => {
            if (updatedFormData[fieldName] === '' || (Array.isArray(updatedFormData[fieldName]) && updatedFormData[fieldName].length === 0)) {
                delete updatedFormData[fieldName];
            }
        });

        setJobFields(updatedJobFields);
        setFormData(updatedFormData);
    };

    const handleSubmit = async () => {
        const jobData = jobFields.map((jobId) => {
            const jobType = formData[`job-${jobId}-type`];
            const emissions = formData[`job-${jobId}-emissions`];
            return { jobType, emissions };
        });

        try {
            const response = await axios.post('YOUR_API_URL', JSON.stringify(jobData), {
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            console.log(response.data);
        } catch (error) {
            console.error(JSON.stringify(jobData));
        }
    };

    return (
        <div className="container">
            <div className="form-container">
                {jobFields.map((jobId) => (
                    <div key={jobId} className="job-field">
                        <JobField
                            jobId={jobId}
                            formData={formData}
                            handleInputChange={handleInputChange}
                            handleDeleteJob={() => handleDeleteJob(jobId)}
                        />
                    </div>
                ))}
                <button
                    className="btn btn-primary button"
                    onClick={handleAddJob}
                >
                    Додати тип роботи
                </button>
                <button
                    className="btn btn-success button"
                    onClick={handleSubmit}
                >
                    Надіслати
                </button>
            </div>
        </div>
    );
}

export default Form;

